package controllers;

import java.util.Comparator;

import models.Donation;

public class DonationComparator implements Comparator<Donation> {

	@Override
	public int compare(Donation d0, Donation d1) {

		return Long.compare(d1.recieved, d0.recieved);
	}

}
