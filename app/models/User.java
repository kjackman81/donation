package models;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;
import play.db.jpa.Blob;

@Entity
public class User extends Model {

	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public Boolean usaCitizen;

	@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	public List<Donation> donations = new ArrayList<Donation>();

	public User(String firstName, String lastName, String email,
			String password, boolean citizen) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.usaCitizen = citizen;
	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	public void addDonation(long recieved, String methodDonated) {

		Donation bal = new Donation(this, recieved, methodDonated);
		bal.save();
		donations.add(bal);

	}

	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

}
