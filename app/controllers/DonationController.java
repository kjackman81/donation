package controllers;

import play.*;
import play.mvc.*;
import java.util.*;

import org.json.simple.JSONObject;

import models.*;

public class DonationController extends Controller {

	public static void index() {
		User user = Accounts.getCurrentUser();

		if (user == null) {
			Logger.info("Donation class index: Unable to getCurrentUser");
			Accounts.login();
		} else {
			String prog = getPercentageTargetAchieved();
			String progress = prog + "%";
			Logger.info("Donation controller : percent target recieved "
					+ progress);
			render(user, progress);
		}

	}

	private static long getDonationTarget() {
		return 20000;
	}

	private static String getPercentageTargetAchieved() {

		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation d : allDonations) {
			total += d.recieved;
		}

		long target = getDonationTarget();
		long percent = 100;
		long percentageAchieved = (total * percent / target);
		
		percentageAchieved = percentageAchieved <= percent ?  percentageAchieved : percent;
		
		String progress = String.valueOf(percentageAchieved);

		return progress;
	}

	public static void donateAmount(long recieved, String methodDonated) {
		Logger.info("amount donated: " + recieved + "  paymentMethod: "
				+ methodDonated);

		// String userId = session.get("logged_in_user");
		// User user = User.findById(Long.parseLong(userId));

		String paypal = "paypal";

		if (methodDonated == null || methodDonated.isEmpty()) {
			methodDonated = paypal;
		}

		User user = Accounts.getCurrentUser();

		if (user == null) {
			Logger.info(
					"Donation class donateAmount: is unable to getCurrentUser");
			Accounts.login();
		} else {
			user.addDonation(recieved, methodDonated);
		
			JSONObject obj = new JSONObject();
			
		    obj.put("progress", getPercentageTargetAchieved());
		    
		    renderJSON(obj);
		}
		

	}

	/*
	 * private static void addDonation(User user, long recieved, String
	 * methodDonated) {
	 * 
	 * Donation bal = new Donation(user, recieved, methodDonated);
	 * user.donations.add(bal); bal.save(); }
	 */

	public static void donationReport() {

		List<User> donators = new ArrayList<User>();
		List<User> donationMakers = User.findAll();

		for (User u : donationMakers) {
			if (u.donations.size() > 0) {
				donators.add(u);
			}

		}
		for (User u : donators) {
			Logger.info(u.firstName);
			Collections.sort(u.donations, new DonationComparator());
		}

		Collections.sort(donators, new DonationTotalAmountComparator());
		render(donators);
	}

	/*
	 * public static void donationReport() { List<Donation> donations =
	 * Donation.findAll(); //Collections.sort(donations, new
	 * DonationAmountComparator()); render(donations); }
	 */

}
