package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;
import java.util.*;

import models.*;

public class Accounts extends Controller {

	public static void signup() {
		render();
	}

	public static void login() {
		Logger.info("landed at login page");
		render();
	}

	public static void logout() {
		session.clear();
		Welcome.index();
	}

	public static void register(String firstName, String lastName, String email,
			String password, boolean citizen) {
		Logger.info(firstName + " " + lastName + " " + email + " " + password);

		User user = new User(firstName, lastName, email, password, citizen);
		user.save();

		login();
	}

	public static void authenticate(String email, String password) {
		Logger.info(
				"Attempting to authenticate with " + email + ":" + password);

		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) {

			session.put("logged_in_userid", user.id);
			Logger.info("You are good to GO!!!:  " + user.firstName + " "
					+ user.lastName);

			DonationController.index();
		} else {
			Logger.info("Authentication failed");
			Welcome.index();
		}
	}

	public static User getCurrentUser() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			return null;

		}

		User logged_in_user = User.findById(Long.parseLong(userId));
		Logger.info("the logged in user is: " + logged_in_user.firstName);
		return logged_in_user;

	}
}