package controllers;

import java.util.Comparator;
import models.Donation;
import models.User;

public class DonationTotalAmountComparator implements Comparator<User> {

	@Override
	public int compare(User u0, User u1) {

		// return Long.compare(d1.recieved, d0.recieved);
		long total0 = 0;
		long total1 = 0;

		for (Donation d : u0.donations) {
			total0 += d.recieved;
		}

		for (Donation d : u1.donations) {
			total1 += d.recieved;
		}

		return Long.compare(total1, total0);

	}

}
