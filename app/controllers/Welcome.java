package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;
import java.util.*;

import models.*;

public class Welcome extends Controller {

	public static void index() {
		Logger.info("Landed in welcome class");
		render();

	}

}