package models;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Donation extends Model {

	public long recieved;
	public String methodDonated;
	public Date date;

	@ManyToOne
	public User from;

	public Donation(User from, long recieved, String methodDonated) {
		this.from = from;
		this.recieved = recieved;
		this.methodDonated = methodDonated;
		date = new Date();

	}

}
